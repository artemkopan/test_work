package com.example.user.testwork.Utils;


public class Const {

    public static final String SHARED_PREF = "SHARED_PREF";

    public static final String FB_TOKEN = "FB_TOKEN";

    public static final String BACKEND_TOKEN = "BACKEND_TOKEN";

}
