package com.example.user.testwork.Server;



public class ServerConst {

    public static final String API_URL = "http://test-api.live.gbksoft.net/api/v1/";

    public static final String API_LOCATION = "location";

    public static final String API_USERS = "users";


    public static final String PARAM_TOKEN = "token";


    public static final String PARAM_LAT = "lat";
    public static final String PARAM_LON = "lon";
    public static final String PARAM_PAGE = "page";
    public static final String PARAM_PER_PAGE = "per-page";


}
