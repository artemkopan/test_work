package com.example.user.testwork.Main;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.example.user.testwork.Login.LoginFragment;
import com.example.user.testwork.R;
import com.example.user.testwork.Server.GetData;
import com.example.user.testwork.Utils.Const;
import com.facebook.FacebookSdk;


public class MainActivity extends FragmentActivity {

    private LoginFragment loginFragment;
    private SharedPreferences sPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sPref = getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE);


        if(sPref.getString(Const.FB_TOKEN, "").isEmpty()){
            initFBLogin(savedInstanceState);
        }


        new GetData().execute();

        setContentView(R.layout.activity_main);
    }


    private void initFBLogin(Bundle savedInstanceState){

        FacebookSdk.sdkInitialize(getApplicationContext());

        if (savedInstanceState == null) {
            // Add the fragment on initial activity setup
            loginFragment = new LoginFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frag_container, loginFragment)
                    .commit();
        } else {
            // Or set the fragment from restored state info
            loginFragment = (LoginFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.frag_container);
        }
    }

}
