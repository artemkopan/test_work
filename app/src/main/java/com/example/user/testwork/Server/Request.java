package com.example.user.testwork.Server;


import android.net.Uri;

public class Request {

    public String query;
    public UrlGetStream.METHOD_TYPE method_type;
    public Uri.Builder uriBuilder;

    private Request(Builder builder) {
        query = builder.query;
        method_type = builder.method_type;
        uriBuilder = builder.uriBuilder;
    }

    public static class Builder {

        private String query;
        private UrlGetStream.METHOD_TYPE method_type;

        private String apiMethod;
        private Uri.Builder uriBuilder;

        public Builder apiMethod(String value) {
            this.apiMethod = value;
            return this;
        }

        public Builder appendParameter(String key, String value) {
            if (uriBuilder == null)
                uriBuilder = new Uri.Builder();

            uriBuilder.appendQueryParameter(key, value);
            return this;
        }

        public Request buildPost(String token) {
            method_type = UrlGetStream.METHOD_TYPE.POST;

            query = ServerConst.API_URL + apiMethod + "?" + ServerConst.PARAM_TOKEN + "=" + token;

            return new Request(this);
        }

        public Request buildGet(String token){
            method_type = UrlGetStream.METHOD_TYPE.GET;

            query = ServerConst.API_URL + apiMethod + "?";
            query += uriBuilder.build().getEncodedQuery();
            query += "&" + ServerConst.PARAM_TOKEN + "=" + token;

            return new Request(this);
        }
    }

}
