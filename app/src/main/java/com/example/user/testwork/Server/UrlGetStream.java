package com.example.user.testwork.Server;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class UrlGetStream {

    public enum METHOD_TYPE { POST, GET }

    public static String getJson(Request backendRequest){

        String result = null;
        URL url = null;
        HttpURLConnection conn = null;
        try {
            url = new URL(backendRequest.query);

            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setDoInput(true);

            if(backendRequest.method_type == METHOD_TYPE.POST){
                conn.setRequestMethod("POST");

                conn.setDoOutput(true);

                String encodedQuery = backendRequest.uriBuilder.build().getEncodedQuery();

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(encodedQuery);
                writer.flush();
                writer.close();
                os.close();

            } else if(backendRequest.method_type == METHOD_TYPE.GET){
                conn.setRequestMethod("GET");
            }

            InputStream inputStream = conn.getInputStream();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] content = new byte[256];
            int bytesRead = -1;

            while((bytesRead = inputStream.read(content)) != -1) {
                baos.write(content, 0, bytesRead);
            }

            result = baos.toString();
            inputStream.close();
            baos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        return result;
    }

}
