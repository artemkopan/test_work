package com.example.user.testwork.Server;

import android.os.AsyncTask;



public class GetData extends AsyncTask<Void, Void, Void> {
    @Override
    protected Void doInBackground(Void... params) {

        UrlGetStream.getJson(new Request.Builder()
                .apiMethod(ServerConst.API_LOCATION)
                .appendParameter(ServerConst.PARAM_LAT, "2")
                .appendParameter(ServerConst.PARAM_LON, "3")
                .buildPost("X52s9rF-HVZ7H44LBS8QXGZUnG8ai3qM"));

        UrlGetStream.getJson(new Request.Builder()
                .apiMethod(ServerConst.API_USERS)
                .appendParameter(ServerConst.PARAM_PAGE, "1")
                .appendParameter(ServerConst.PARAM_PER_PAGE, "3")
                .buildGet("X52s9rF-HVZ7H44LBS8QXGZUnG8ai3qM"));

        return null;
    }
}
