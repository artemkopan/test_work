package com.example.user.testwork.Login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.testwork.R;
import com.example.user.testwork.Utils.Const;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public class LoginFragment extends Fragment{

    private View view;
    private CallbackManager callbackManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.login_fragment, container, false);

        callbackManager = CallbackManager.Factory.create();

        initLoginButton();

        return view;
    }


    private void initLoginButton(){

        LoginButton authButton = (LoginButton) view.findViewById(R.id.login_btn);
        authButton.setFragment(this);
        authButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("TOKEN", loginResult.getAccessToken().getToken());
                getActivity().getSharedPreferences(Const.SHARED_PREF, Context.MODE_PRIVATE).edit().putString(Const.FB_TOKEN, loginResult.getAccessToken().getToken()).commit();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
